'use strict';

describe( 'LocationService', function () {

	beforeEach( module( 'VenueApp' ) );

	var LocationService, $rootScope, $window;

	beforeEach( inject( function ( _LocationService_, _$window_, _$rootScope_ ) {
		LocationService = _LocationService_;
		$window = _$window_;
		$rootScope = _$rootScope_;
	} ) );

	it( 'should get unsupported browser error message', function () {
		// TODO: Check why $window mock is not working?
		$window = { navigator : false }; // condition 1 should fail
		var result;
		LocationService.getLocation().then( function() {}, function( error ) {
			result = error;
		} );
		expect( result ).toBeDefined();		
		$window = { navigator : { geolocation: false } }; // condition 2 should fail
		var result;
		LocationService.getLocation().then( function() {}, function( error ) {
			result = error;
		} );
		expect( result ).toBeDefined();
	});
	
	it( 'should get location with coordinates', function () {
		var expectedPosition = { coords: { latitude: 65, longitude: 25 } };
		spyOn( $window.navigator.geolocation, 'getCurrentPosition' ).and.callFake( function() {
			arguments[0]( expectedPosition );
		} );
		var result;
		LocationService.getLocation().then( function( data ) {
		  result = data;
		});
		$rootScope.$digest();
		expect( result ).toEqual( expectedPosition );
	});

	it( 'should get geolocation error message', function () {
		var expectedError = { code: 1, message : 'error' };
		spyOn( $window.navigator.geolocation, 'getCurrentPosition' ).and.callFake( function() {
			arguments[1]( expectedError );
		} );
		var result;
		LocationService.getLocation().then( function() {}, function( error ) {
			result = error;
		} );
		$rootScope.$digest();
		expect( result ).toEqual( expectedError.message );
	});

});
