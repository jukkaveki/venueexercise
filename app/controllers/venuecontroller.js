angular.module('VenueApp').controller( 'VenueController', [ '$scope', 'LocationService', 'VenueService', 'AddressService',
	function( $scope, LocationService, VenueService, AddressService ) {
	
	$scope.lat = -1;
	$scope.lng = -1;
	$scope.initialized = false;
	$scope.initializationError = '';
	$scope.searchError = '';
	$scope.venues = [];
	$scope.query = '';
	$scope.address = '';

	$scope.isInitialized = function() {
		return $scope.initialized;
	}
	
	$scope.hasLocation = function() {
		return $scope.lat != -1 && $scope.lng != -1;
	}
	
	$scope.hasInitializationError = function() {
		return $scope.initializationError != '';
	}
	
	$scope.hasSearchError = function() {
		return $scope.searchError != '';
	}
	
	$scope.resolveAddress = function() {
		AddressService.getNearestAddress( $scope.lat, $scope.lng ).then( function ( address ) {
			$scope.address = address;
		});
	}
	
	$scope.hasAddress = function() {
		return $scope.address != '';
	}
	
	$scope.search = function() {
		if ( $scope.query != '' ) {
			VenueService.getVenues( $scope.lat, $scope.lng, $scope.query ).then(
			function( venues ) {
				$scope.venues = venues;
				$scope.searchError = '';
			}, function( error ) { 
				$scope.searchError = error;
			} );
		} else {
			$scope.venues = [];
		}
	}
	
	LocationService.getLocation().then( 
		function( position ) { 
			$scope.lat = position.coords.latitude;
			$scope.lng = position.coords.longitude;
			$scope.initializationError = '';
			$scope.initialized = true;
			$scope.resolveAddress();
		}, function( error ) {
			$scope.initializationError = error;
			$scope.initialized = true;
		} );

} ] );