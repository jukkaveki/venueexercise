angular.module('VenueApp').factory( 'VenueService', [ '$q', '$http', function( $q, $http ) {
	return {
		getVenues: function( lat, lng, query ) {
			var url = 'https://api.foursquare.com/v2/venues/search?client_id=CYEMKOM4OLTP5PHMOFVUJJAMWT5CH5G1JBCYREATW21XLLSZ&client_secret=GYNP4URASNYRNRGXR5UEN2TGTKJHXY5FGSAXTIHXEUG1GYM2&v=20150408&ll=' + lat + ',' + lng + '&query=' + query;
			return $http.get( url )
				.then( function( response ) {
					if ( response.data.meta.code == '200' ) {
						return response.data.response.venues;
					} else {
						return $q.reject( response.data.meta.code + ' ' + response.data.meta.errorDetail );
					}
				}, function( error ) {
					return $q.reject( 'HTTP ' + error.status + ' ' + error.data );
				} );
		}
	};
} ] );