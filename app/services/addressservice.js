angular.module('VenueApp').factory( 'AddressService', [ '$q', '$http', function( $q, $http ) {
	return {
		getNearestAddress: function( lat, lng ) {
			var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng;
			return $http.get( url )
				.then( function( response ) {
					if (response.data.results.length > 0) {
						return response.data.results[0].formatted_address;
					} else {
						return '';
					}
				}, function( error ) {
					return $q.reject( 'HTTP ' + error.status + ' ' + error.data );
				} );
		}
	};
} ] );