'use strict';

describe( 'VenueController', function () {

	var $controller, $q, $scope, LocationService, VenueService, AddressService;

	beforeEach( module( 'VenueApp' ) );
	
	beforeEach( inject( function ( _$q_, _$controller_, $rootScope, _LocationService_, _VenueService_, _AddressService_ ) {
		$q = _$q_;
		$controller = _$controller_;
		$scope = $rootScope.$new();
		LocationService = _LocationService_;
		VenueService = _VenueService_;
		AddressService = _AddressService_;
	} ) );
	
	it( 'should not be initially initialized', function () {
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService } );
		expect( $scope.isInitialized() ).toEqual( false );
	});

	it( 'should be initialized after location service returns location', function () {
		var expectedLocation = { coords: { latitude: 65, longitude: 25 } };
		var expectedAddress = 'address';
		spyOn( LocationService, 'getLocation' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedLocation );
			return deferred.promise;
		});
		spyOn( AddressService, 'getNearestAddress' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedAddress );
			return deferred.promise;
		});
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService, AddressService : AddressService } );
		$scope.$digest();
		
		expect( LocationService.getLocation ).toHaveBeenCalled();
		expect( $scope.isInitialized() ).toEqual( true );
		expect( $scope.hasInitializationError() ).toEqual( false );
		expect( $scope.hasLocation() ).toEqual( true );
		expect( $scope.lat ).toEqual( expectedLocation.coords.latitude );
		expect( $scope.lng ).toEqual( expectedLocation.coords.longitude );
		expect( AddressService.getNearestAddress ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude );
		expect( $scope.hasAddress() ).toEqual( true );
		expect( $scope.address ).toEqual( expectedAddress );
	});

	it( 'should be initialized after location service returns error', function () {
		var expectedError = 'error';
		spyOn( LocationService, 'getLocation' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.reject( expectedError );
			return deferred.promise;
		});
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService } );
		$scope.$digest();
		expect( LocationService.getLocation ).toHaveBeenCalled();
		expect( $scope.isInitialized() ).toEqual( true );
		expect( $scope.hasInitializationError() ).toEqual( true );
		expect( $scope.hasLocation() ).toEqual( false );
		expect( $scope.initializationError ).toEqual( expectedError );
	});
	
	it( 'should not have venues initially', function () {
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService } );
		expect( $scope.venues ).toEqual( [] );
	});	

	it( 'should have venues after venue service returns venues', function () {
		var expectedLocation = { coords: { latitude: 65, longitude: 25 } };
		spyOn( LocationService, 'getLocation' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedLocation );
			return deferred.promise;
		});
		spyOn( AddressService, 'getNearestAddress' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( 'address' );
			return deferred.promise;
		});
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService } );
		$scope.$digest();
		expect( LocationService.getLocation ).toHaveBeenCalled();
		expect( $scope.isInitialized() ).toEqual( true );
		expect( AddressService.getNearestAddress ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude );
		var expectedVenues = [
			{ 
				"id" : "test-id",
				"name" : "test-name",
				"contact" : {},
				"location" : { 
					"lat" : 65, 
					"lng" : 25, 
					"distance": 123, 
					"cc" : "FI" , 
					"country" : "Test Country" , 
					"formattedAddress" : [ "Test Country" ] },
				"categories" : [ { 
					"id" : "test-cat-id", 
					"name" : "Test Venue", 
					"pluralName" : "Test Venues" , 
					"shortName" : "Test" , 
					"icon" : { 
						"prefix" : "", 
						"suffix" : "" 
					}, 
				"primary" : true } ],
				"verified" : true, 
				"stats" : { "checkinsCount": 1, "usersCount" : 2, "tipCount" : 3 },
				"specials" : { "count" : 1, "items" : [] },
				"hereNow" : { "count" : 0, "summary" : "Nobody here", "groups" : [] },
				"referralId" : "v-12345678"
			}		
		];
		spyOn( VenueService, 'getVenues' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedVenues );
			return deferred.promise;
		});		
		$scope.query = 'test';
		$scope.search();
		$scope.$digest();
		expect( VenueService.getVenues ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude, $scope.query );
		expect( $scope.hasSearchError() ).toEqual( false );
		expect( $scope.venues ).toEqual( expectedVenues );
	});
	
	it( 'should not have venues after venue service returns error', function () {
		var expectedLocation = { coords: { latitude: 65, longitude: 25 } };
		spyOn( LocationService, 'getLocation' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedLocation );
			return deferred.promise;
		});
		spyOn( AddressService, 'getNearestAddress' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( 'address' );
			return deferred.promise;
		});
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService } );
		$scope.$digest();
		expect( LocationService.getLocation ).toHaveBeenCalled();
		expect( $scope.isInitialized() ).toEqual( true );
		expect( AddressService.getNearestAddress ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude );		var expectedError = 'error';
		spyOn( VenueService, 'getVenues' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.reject( expectedError );
			return deferred.promise;
		});		
		$scope.query = 'test';
		$scope.search();
		$scope.$digest();
		expect( VenueService.getVenues ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude, $scope.query );
		expect( $scope.hasSearchError() ).toEqual( true );
		expect( $scope.searchError ).toEqual( expectedError );		
		expect( $scope.venues ).toEqual( [] );
	});

	it( 'should not have venues if query is empty', function () {
		var expectedLocation = { coords: { latitude: 65, longitude: 25 } };
		spyOn( LocationService, 'getLocation' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedLocation );
			return deferred.promise;
		});
		spyOn( AddressService, 'getNearestAddress' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( 'address' );
			return deferred.promise;
		});
		$controller( 'VenueController', { $scope : $scope, LocationService : LocationService, VenueService : VenueService } );
		$scope.$digest();
		expect( LocationService.getLocation ).toHaveBeenCalled();
		expect( $scope.isInitialized() ).toEqual( true );
		expect( AddressService.getNearestAddress ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude );
		var expectedVenues = [
			{ 
				"id" : "test-id",
				"name" : "test-name",
				"contact" : {},
				"location" : { 
					"lat" : 65, 
					"lng" : 25, 
					"distance": 123, 
					"cc" : "FI" , 
					"country" : "Test Country" , 
					"formattedAddress" : [ "Test Country" ] },
				"categories" : [ { 
					"id" : "test-cat-id", 
					"name" : "Test Venue", 
					"pluralName" : "Test Venues" , 
					"shortName" : "Test" , 
					"icon" : { 
						"prefix" : "", 
						"suffix" : "" 
					}, 
				"primary" : true } ],
				"verified" : true, 
				"stats" : { "checkinsCount": 1, "usersCount" : 2, "tipCount" : 3 },
				"specials" : { "count" : 1, "items" : [] },
				"hereNow" : { "count" : 0, "summary" : "Nobody here", "groups" : [] },
				"referralId" : "v-12345678"
			}		
		];
		spyOn( VenueService, 'getVenues' ).and.callFake( function() {
			var deferred = $q.defer();
			deferred.resolve( expectedVenues );
			return deferred.promise;
		});		
		$scope.query = 'test';
		$scope.search();
		$scope.$digest();
		expect( VenueService.getVenues ).toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude, $scope.query );
		expect( $scope.hasSearchError() ).toEqual( false );
		expect( $scope.venues ).toEqual( expectedVenues );
		
		$scope.query = '';
		$scope.search();
		$scope.$digest();

		expect( VenueService.getVenues ).not.toHaveBeenCalledWith( expectedLocation.coords.latitude, expectedLocation.coords.longitude, $scope.query );
		expect( $scope.hasSearchError() ).toEqual( false );
		expect( $scope.venues ).toEqual( [] );		
	});
	
});