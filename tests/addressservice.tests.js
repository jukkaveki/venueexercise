'use strict';

describe( 'AddressService', function () {

	beforeEach( module( 'VenueApp' ) );

	var AddressService, $http, $httpBackend;

	beforeEach( inject( function ( _AddressService_, _$http_, _$httpBackend_ ) {
		AddressService = _AddressService_;
		$http = _$http_;
		$httpBackend = _$httpBackend_;
	} ) );
	
	afterEach( function() {
		$httpBackend.verifyNoOutstandingExpectation();
		$httpBackend.verifyNoOutstandingRequest();
    } );
	
	it( 'should get nearest address', function () {
		var expectedResponseData = { "results" : [
			{
				"address_components" : [
					{
						"long_name" : "1",
						"short_name" : "1",
						"types" : [ "street_number" ]
					},
					{
						"long_name" : "test street",
						"short_name" : "test street",
						"types" : [ "route" ]
					},
					{
						"long_name" : "test city",
						"short_name" : "test city",
						"types" : [ "administrative_area_level_3", "political" ]
					},
					{
						"long_name" : "test country",
						"short_name" : "TC",
						"types" : [ "country", "political" ]
					},
					{
						"long_name" : "12345",
						"short_name" : "12345",
						"types" : [ "postal_code" ]
					}
				],
				"formatted_address" : "test street 1, 12345 test city, test country",
				"geometry" : {
					"location" : {
						"lat" : 65.0758196,
						"lng" : 25.4602003
					},
					"location_type" : "ROOFTOP",
					"viewport" : {
						"northeast" : {
							"lat" : 65.07716858029151,
							"lng" : 25.46154928029151
						},
						"southwest" : {
							"lat" : 65.07447061970849,
							"lng" : 25.4588513197085
						}
					}
				},
				"place_id" : "ChIJ4wj8Z1UsgEYRHDri_FOO7zo",
				"types" : [ "street_address" ]
			} ] }
		$httpBackend.when( 'GET' ).respond( expectedResponseData );
		var result;
		AddressService.getNearestAddress( 65, 25 ).then( function( data ) {
		  result = data;
		});
		$httpBackend.flush();
		expect( result ).toEqual( expectedResponseData.results[0].formatted_address );
	});

	it( 'should get not get nearest address', function () {
		var expectedResponseData = { "results" : [] }
		$httpBackend.when( 'GET' ).respond( expectedResponseData );
		var result;
		AddressService.getNearestAddress( 65, 25 ).then( function( data ) {
		  result = data;
		});
		$httpBackend.flush();
		expect( result ).toEqual( '' );
	});
	
	it( 'should get http error', function () {
		var httpErrorCode = 404;
		var httpErrorMsg = "NOT FOUND";
		$httpBackend.when( 'GET' ).respond( httpErrorCode, httpErrorMsg );
		var result;
		AddressService.getNearestAddress( 65, 25 ).then( function() {}, function( error ) { 
			result = error; 
		} );
		$httpBackend.flush();
		expect( result ).toEqual( 'HTTP ' + httpErrorCode + ' ' + httpErrorMsg );
	});
	
});