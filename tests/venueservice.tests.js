'use strict';

describe( 'VenueService', function () {

	beforeEach( module( 'VenueApp' ) );

	var VenueService, $http, $httpBackend;

	beforeEach( inject( function ( _VenueService_, _$http_, _$httpBackend_ ) {
		VenueService = _VenueService_;
		$http = _$http_;
		$httpBackend = _$httpBackend_;
	} ) );
	
	afterEach( function() {
		$httpBackend.verifyNoOutstandingExpectation();
		$httpBackend.verifyNoOutstandingRequest();
    } );
	
	it( 'should get list of venues', function () {
		var expectedResponseData = { "meta" : { "code" : 200 },
						 "response": { "venues": [ { 
							"id" : "test-id",
							"name" : "test-name",
							"contact" : {},
							"location" : { 
								"lat" : 65, 
								"lng" : 25, 
								"distance": 123, 
								"cc" : "FI" , 
								"country" : "Test Country" , 
								"formattedAddress" : [ "Test Country" ] },
							"categories" : [ { 
								"id" : "test-cat-id", 
								"name" : "Test Venue", 
								"pluralName" : "Test Venues" , 
								"shortName" : "Test" , 
								"icon" : { 
									"prefix" : "", 
									"suffix" : "" 
								}, 
							"primary" : true } ],
							"verified" : true, 
							"stats" : { "checkinsCount": 1, "usersCount" : 2, "tipCount" : 3 },
							"specials" : { "count" : 1, "items" : [] },
							"hereNow" : { "count" : 0, "summary" : "Nobody here", "groups" : [] },
							"referralId" : "v-12345678" } ] } };
		$httpBackend.when( 'GET' ).respond( expectedResponseData );
		var result;
		VenueService.getVenues( 65, 25, 'test' ).then( function( data ) {
		  result = data;
		});
		$httpBackend.flush();
		expect( result ).toEqual( expectedResponseData.response.venues );
	});

	it( 'should get http error', function () {
		var httpErrorCode = 404;
		var httpErrorMsg = "NOT FOUND";
		$httpBackend.when( 'GET' ).respond( httpErrorCode, httpErrorMsg );
		var result;
		VenueService.getVenues( 65, 25, 'test' ).then( function() {}, function( error ) { 
			result = error; 
		} );
		$httpBackend.flush();
		expect( result ).toEqual( 'HTTP ' + httpErrorCode + ' ' + httpErrorMsg );
	});

	it( 'should get service error', function () {
		var errorCode = 404;
		var errorMsg = "NOT FOUND";
		var expectedResponseData = { "meta" : { "code" : errorCode, "errorDetail" : errorMsg },
						 "response": { "venues": [] } };
		$httpBackend.when( 'GET' ).respond( expectedResponseData );
		var result;
		VenueService.getVenues( 65, 25, 'test' ).then( function() {}, function( error ) { 
			result = error; 
		} );
		$httpBackend.flush();
		expect( result ).toEqual( errorCode + ' ' + errorMsg );
	});
	
});