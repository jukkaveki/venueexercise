angular.module('VenueApp').factory( 'LocationService', [ '$q', '$rootScope', '$window', function( $q, $rootScope, $window ) {
	return {
		getLocation: function() {
			var deferred = $q.defer();
			if ( $window.navigator && $window.navigator.geolocation ) {
				$window.navigator.geolocation.getCurrentPosition( 
					function( position ) {
						$rootScope.$apply( function() { 
							deferred.resolve( position );
						} );
					}, 
					function( error ) {
						$rootScope.$apply( function() { 
							deferred.reject( error.message );
						});
					}
				);
			} else {
				$rootScope.$apply( function() { 
					deferred.reject( 'Browser does not support Geolocation!' );
				});
			}
			return deferred.promise;
		}
	};
} ] );